package handlers

import "net/http"

func apiHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("You have reached Echorand Corp’s test API"))
}
