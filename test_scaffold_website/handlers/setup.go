package handlers

import (
	"net/http"
)

func Setup() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/api", apiHandler)
	mux.HandleFunc("/healthcheck", healthCheckHandler)
	
	
	mux.HandleFunc("/", indexHandler)
	fs := http.FileServer(http.Dir("static"))
	mux.Handle("/static/", http.StripPrefix("/static/", fs))
	

	return mux
}
