package main

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestGenerateScaffold(t *testing.T) {

	testCases := []struct {
		conf           projectConfig
		expectedOutput string
	}{
		{
			conf: projectConfig{
				Name:         "MyProject",
				localPath:    "/path/to/dir",
				RepoURL:      "github.com/username/myproject",
				StaticAssets: false,
			},
			expectedOutput: "Generating scaffold for project MyProject in /path/to/dir",
		},
	}
	byteBuf := new(bytes.Buffer)
	for _, tc := range testCases {
		generateScaffold(byteBuf, tc.conf)
		if len(tc.expectedOutput) != 0 {
			gotOutput := byteBuf.String()
			if strings.Index(gotOutput, tc.expectedOutput) == -1 {
				t.Errorf("Expected output: %s, Got: %s", tc.expectedOutput, gotOutput)
			}
		}
		byteBuf.Reset()
	}
}

func TestGeneratedScaffold(t *testing.T) {
	tempDir := t.TempDir()
	testDir := filepath.Join(tempDir, "test_webapp")
	expectedDir := "./test_scaffold_website"

	if err := generateScaffold(ioutil.Discard, projectConfig{
		localPath:    testDir,
		Name:         "test",
		RepoURL:      "github.com/username/test",
		StaticAssets: true,
	}); err != nil {
		t.Fatal(err)
	}

	expectedFileContents := map[string]string{
		filepath.Join(testDir, "go.mod"):                      filepath.Join(expectedDir, "go.mod"),
		filepath.Join(testDir, "server.go"):                   filepath.Join(expectedDir, "server.go"),
		filepath.Join(testDir, "handlers", "api.go"):          filepath.Join(expectedDir, "handlers/api.go"),
		filepath.Join(testDir, "handlers", "index.go"):        filepath.Join(expectedDir, "handlers/index.go"),
		filepath.Join(testDir, "handlers", "setup.go"):        filepath.Join(expectedDir, "handlers/setup.go"),
		filepath.Join(testDir, "handlers", "healthcheck.go"):  filepath.Join(expectedDir, "handlers/healthcheck.go"),
		filepath.Join(testDir, "static", "css", "styles.css"): filepath.Join(expectedDir, "static/css/styles.css"),
		filepath.Join(testDir, "static", "js", "index.js"):    filepath.Join(expectedDir, "static/js/index.js"),
	}

	for gotFile, expFile := range expectedFileContents {
		got, err := os.ReadFile(gotFile)
		if err != nil {
			t.Fatalf("couldn't read %q: %v", gotFile, err)
		}

		exp, err := os.ReadFile(gotFile)
		if err != nil {
			t.Fatalf("couldn't read %q: %v", expFile, err)
		}

		if got, exp := string(got), string(exp); got != exp {
			t.Fatalf("content of %q and %q doesn't match", gotFile, expFile)
		}
	}
}
